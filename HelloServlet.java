package mainPackage;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String val = req.getParameter("item");
		AddMethods.write(val);
		resp.sendRedirect("/index.jsp");
	}


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String lineNr = req.getParameter("line");
		DelMethods.delLines(new Integer(lineNr));
		resp.sendRedirect("/index.jsp");
	}
}

