package mainPackage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AddMethods {
    public static String LI_DIR = "src/main/java/mainPackage/items.txt";

    public static void write(String inPut) {
        try {
            PrintWriter pen = new PrintWriter(new FileOutputStream(LI_DIR, true));
            pen.println(inPut);
            pen.flush();
            pen.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<String> writeWhole() {
        List<String> lines = new ArrayList<>();
        String line;
        try {
            BufferedReader slide = new BufferedReader(new FileReader(LI_DIR));
            while ((line = slide.readLine()) != null) {
                lines.add(line);
            }
            slide.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }


}
