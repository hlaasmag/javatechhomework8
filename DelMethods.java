package mainPackage;

import java.util.List;

import static mainPackage.CreateMethod.newFile;
import static mainPackage.AddMethods.writeWhole;

public class DelMethods {

    public static void delLines(int byNumber) {
        List<String> lines = writeWhole();
        lines.remove(byNumber);
        newFile(lines);
    }

    public static void delAll(String search) {
        List<String> lines = writeWhole();

        for (int lineNr = 0; lineNr < lines.size(); lineNr++) {
            if (lines.get(lineNr).equals(search)) {
                lines.remove(lineNr);
                lineNr--;
            }
        }
        newFile(lines);
    }

}
